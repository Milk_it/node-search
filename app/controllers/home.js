var paths = {
  inputFile: './app/input.json',
  outputFile: './app/output.json',
  resourceFolder: './app/resources/messages',
  translationSuffix: '-cl',
  englishSuffix: '-uk'
};

var express = require('express'),
  router = express.Router(),
  fs = require('graceful-fs'),
  Promise = require("bluebird"),
  _ = require("lodash"),
  recursive = require('recursive-readdir'),
  readFile = Promise.promisify(fs.readFile),
  input = JSON.parse(fs.readFileSync(paths.inputFile, 'utf8'));

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
  var englishFilePromises = [];
  var foreignFilePromises = [];
  var values = [];

  for (var key in input) {
    var upperValue = input[key];
    if (typeof upperValue == 'string') {
      values.push({upperValue: upperValue, key: key});
    }
  }

  recursive(paths.resourceFolder, function (err, files) {

    files.forEach(function (file) {
      if(file.indexOf(paths.englishSuffix) >= 0) {
        englishFilePromises.push(readFile(file));
      }
      else if(file.indexOf(paths.translationSuffix) >= 0) {
        foreignFilePromises.push(readFile(file));
      }
    });

    Promise.all(englishFilePromises).then(function (dataArray) {
      var output = {};

      values.forEach(function (valueObj) {
        dataArray.forEach(function (data) {
          var stringData = data.toString();
          if (stringData) {
            var rows = stringData.split('\n');

            rows.forEach(function(value) {
              if(value.indexOf('=') > -1) {
                var rowArr = value.trim().split('=');
                var returnCarret = rowArr[1].indexOf('\r');
                if (returnCarret > -1) {
                  rowArr[1].slice(0, returnCarret);
                }
                if(rowArr[1] && rowArr[1].trim().toLowerCase() == valueObj.upperValue.trim().toLowerCase()) {
                  output[valueObj.key] = rowArr[0];
                }
              }
            });
          }
        });
      });

      Promise.all(foreignFilePromises).then(function (dataArray) {
        for(var responsiveKey in output) {
          var mobileKey = output[responsiveKey];
          dataArray.forEach(function (data) {
            var stringData = data.toString();
            if (stringData) {
              var rows = stringData.split('\n');

              rows.forEach(function(value) {
                if(value.indexOf('=') > -1) {
                  var rowArr = value.trim().split('=');
                  if(rowArr[0] && rowArr[0].trim().toLowerCase() == mobileKey.trim().toLowerCase()) {
                    output[responsiveKey] = rowArr[1];
                  }
                }
              });
            }
          });
        }

        fs.writeFileSync(paths.outputFile, JSON.stringify(_.merge(input, output)));

        res.render('index', {
          result: output
        });
      });

    })
  });
});
